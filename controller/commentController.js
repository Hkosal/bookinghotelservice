const pool = require("../config_db");
const queries = require("../lib/queries/queryComments");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

//create comment
const createComment = (req, res) => {
  const commentId = uuid();
  const created = now;
  const { userId, firstname, lastname, phone } = req.body;

  //add comment to db
  pool.query(
    queries.addComment,
    [commentId, userId, firstname, lastname, phone, created],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Comment Created Successfully!");
    }
  );
};

//get comments
const getComments = (req, res) => {
  pool.query(queries.getComments, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get comment by id
const getCommentById = (req, res) => {
  const commentId = req.params.commentId;
  pool.query(queries.getCommentById, [commentId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update comment
const updateComment = (req, res) => {
  const commentId = req.params.commentId;
  const updated = now;
  const { userId, firstname, lastname, phone } = req.body;

  pool.query(queries.getCommentById, [commentId], (error, results) => {
    if (error) throw error;
    const noCommentFound = !results.rows.length;
    if (noCommentFound) {
      res.send("Comment does not exist.");
    }

    //update comment
    pool.query(
      queries.updateComment,
      [userId, firstname, lastname, phone, updated, commentId],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Comment Updated Successfully!");
      }
    );
  });
};

//delete comment
const deleteComment = (req, res) => {
  const commentId = req.params.commentId;

  pool.query(queries.getCommentById, [commentId], (error, results) => {
    if (error) throw error;
    const noCommentFound = !results.rows.length;
    if (noCommentFound) {
      res.send("Comment does not exist.");
    }

    pool.query(queries.deleteComment, [commentId], (error, results) => {
      if (error) throw error;
      res.status(200).send("Comment deleted successfully.");
    });
  });
};

module.exports = {
  createComment,
  getComments,
  getCommentById,
  updateComment,
  deleteComment,
};
