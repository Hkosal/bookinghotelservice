const pool = require("../config_db");
const queries = require("../lib/queries/queryProvinces");
const queryPlaceInProvince = require("../lib/queries/queryPlaces");
const queryHotelInProvince = require("../lib/queries/queryHotels");
const { v4: uuid } = require("uuid");
const { json } = require("body-parser");
const now = new Date().toISOString().slice(0, 10);

//create province
const createProvince = (req, res) => {
  const provinceId = uuid();
  const created = now;
  const { name, nameNative } = req.body;

  //add province to db
  pool.query(
    queries.addProvince,
    [provinceId, name, nameNative, created],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Province Created Successfully!");
    }
  );
};

//get provinces
const getProvinces = (req, res) => {
  pool.query(queries.getProvinces, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get province by id
const getProvinceById = (req, res) => {
  const provinceId = req.params.provinceId;
  pool.query(queries.getProvinceById, [provinceId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update province
const updateProvince = (req, res) => {
  const provinceId = req.params.provinceId;
  const updated = now;
  const { name, nameNative } = req.body;

  pool.query(queries.getProvinceById, [provinceId], (error, results) => {
    if (error) throw error;
    const noProvinceFound = !results.rows.length;
    if (noProvinceFound) {
      res.send("Province does not exist.");
    }

    //update province
    pool.query(
      queries.updateProvince,
      [name, nameNative, updated, provinceId],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Province Updated Successfully!");
      }
    );
  });
};

//delete province
const deleteProvince = (req, res) => {
  const provinceId = req.params.provinceId;

  pool.query(queries.getProvinceById, [provinceId], (error, results) => {
    if (error) throw error;
    const noProvinceFound = !results.rows.length;
    if (noProvinceFound) {
      res.send("Province does not exist.");
    }

    pool.query(queries.deleteProvince, [provinceId], (error, results) => {
      if (error) throw error;
      res.status(200).send("Province deleted successfully.");
    });
  });
};
//get places in province
async function getPlaces(provinceId) {
  return (province = pool.query(queryPlaceInProvince.getPlacesInProvince, [
    provinceId,
  ]));
}
//get hotel in province
async function getHotels(provinceId) {
  return (province = pool.query(queryHotelInProvince.getHotelsInProvince, [
    provinceId,
  ]));
}
//get province
async function getProvince() {
  return (provinces = pool.query(queries.getProvinces));
}

//get province places hotels
async function getProvincePlacesHotels(req, res) {
  const provinces = await getProvince();
  const resultProvince = provinces.rows;
  const resultProvince0 = resultProvince[0];
  const resultProvince1 = resultProvince[1];

  const provinceId0 = resultProvince[0].provinceid;
  const provinceId1 = resultProvince[1].provinceid;

  const places0 = await getPlaces(provinceId0);
  const resultPlace0 = places0.rows;

  const place1 = await getPlaces(provinceId1);
  const resultPlace1 = place1.rows;

  const hotel0 = await getHotels(provinceId0);
  const resultHotel0 = hotel0.rows;

  const hotel1 = await getHotels(provinceId1);
  const resultHotel1 = hotel1.rows;

  resultProvince0["places"] = resultPlace0;
  resultProvince0["hotels"] = resultHotel0;
  resultProvince1["places"] = resultPlace1;
  resultProvince1["hotels"] = resultHotel1;

  const results = [resultProvince0, resultProvince1];
  res.send(results);
}

//get places hotels by province
async function getPlacesHotelsByProvince(req, res) {
  const provinceId = req.params.provinceId;

  const places = await getPlaces(provinceId);
  const resultPlaces = places.rows;

  const hotels = await getHotels(provinceId);
  const resultHotels = hotels.rows;

  const result = [
    {
      key: "1111",
      places: resultPlaces,
      hotels: resultHotels,
    },
  ];
  res.send(result);
}

module.exports = {
  createProvince,
  getProvinces,
  getProvinceById,
  updateProvince,
  deleteProvince,
  getProvincePlacesHotels,
  getPlacesHotelsByProvince,
};
