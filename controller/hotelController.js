const pool = require("../config_db");
const queries = require("../lib/queries/queryHotels");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

//create hotel
const createHotel = (req, res) => {
  const hotelId = uuid();
  const created = now;
  const {
    name,
    nameNative,
    description,
    descriptionNative,
    photo,
    placeId,
    provinceId,
  } = req.body;

  //add hotel to db
  pool.query(
    queries.addHotel,
    [
      hotelId,
      name,
      nameNative,
      description,
      descriptionNative,
      photo,
      placeId,
      provinceId,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Hotel Created Successfully!");
    }
  );
};

//get hotels
const getHotels = (req, res) => {
  pool.query(queries.getHotels, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get hotel by id
const getHotelById = (req, res) => {
  const hotelId = req.params.hotelId;
  pool.query(queries.getHotelById, [hotelId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update hotel
const updateHotel = (req, res) => {
  const hotelId = req.params.hotelId;
  const updated = now;
  const {
    name,
    nameNative,
    description,
    descriptionNative,
    photo,
    placeId,
    provinceId,
  } = req.body;

  pool.query(queries.getHotelById, [hotelId], (error, results) => {
    if (error) throw error;
    const noHotelFound = !results.rows.length;
    if (noHotelFound) {
      res.send("Hotel does not exist.");
    }

    //update hotel
    pool.query(
      queries.updateHotel,
      [
        name,
        nameNative,
        description,
        descriptionNative,
        photo,
        placeId,
        provinceId,
        updated,
        hotelId,
      ],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Hotel Updated Successfully!");
      }
    );
  });
};

//delete hotel
const deleteHotel = (req, res) => {
  const hotelId = req.params.hotelId;

  pool.query(queries.getHotelById, [hotelId], (error, results) => {
    if (error) throw error;
    const noHotelFound = !results.rows.length;
    if (noHotelFound) {
      res.send("Hotel does not exist.");
    }

    pool.query(queries.deleteHotel, [hotelId], (error, results) => {
      if (error) throw error;
      res.status(200).send("Hotel deleted successfully.");
    });
  });
};

//get top hotels in province
const getTopHotelsInProvince = (req, res) => {
  const provinceId = req.params.provinceId;
  pool.query(queries.getHotelsInProvince, [provinceId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    const topPlacesInProvince = [];
    if (sortByDate.length > 0) {
      if (sortByDate.length < 4) {
        for (var i = 0; i < sortByDate.length; i++) {
          topPlacesInProvince.push(sortByDate[i]);
        }
      } else {
        topPlacesInProvince.push(sortByDate[0]);
        topPlacesInProvince.push(sortByDate[1]);
        topPlacesInProvince.push(sortByDate[2]);
        topPlacesInProvince.push(sortByDate[3]);
      }
    }
    res.status(200).json(topPlacesInProvince);
  });
};

//get hotels in province
const getHotelsInProvince = (req, res) => {
  const provinceId = req.params.provinceId;
  pool.query(queries.getHotelsInProvince, [provinceId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get top hotels in place
const getTopHotelsInPlace = (req, res) => {
  const placeId = req.params.placeId;
  pool.query(queries.getHotelsInPlace, [placeId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    const topPlacesInPlace = [];
    if (sortByDate.length > 0) {
      if (sortByDate.length < 4) {
        for (var i = 0; i < sortByDate.length; i++) {
          topPlacesInPlace.push(sortByDate[i]);
        }
      } else {
        topPlacesInPlace.push(sortByDate[0]);
        topPlacesInPlace.push(sortByDate[1]);
        topPlacesInPlace.push(sortByDate[2]);
        topPlacesInPlace.push(sortByDate[3]);
      }
    }
    res.status(200).json(topPlacesInPlace);
  });
};

//get hotels in place
const getHotelsInPlace = (req, res) => {
  const placeId = req.params.placeId;
  pool.query(queries.getHotelsInPlace, [placeId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

module.exports = {
  createHotel,
  getHotels,
  getHotelById,
  updateHotel,
  deleteHotel,
  getTopHotelsInProvince,
  getHotelsInProvince,
  getTopHotelsInPlace,
  getHotelsInPlace,
};
