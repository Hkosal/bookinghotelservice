const pool = require("../config_db");
const queries = require("../lib/queries/queryBookings");
const queriesNotification = require("../lib/queries/queryNotifications");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

// create Notification
async function notification(
  bookingId,
  name,
  nameNative,
  description,
  descriptionNative,
  photo,
  hotelId,
  userId,
  status,
  created
) {
  const notificationId = uuid();
  const notificationDescription = "You booking have been successful.";

  //add notification to db
  return (createNoti = pool.query(queriesNotification.addNotification, [
    notificationId,
    notificationDescription,
    bookingId,
    name,
    nameNative,
    description,
    descriptionNative,
    photo,
    hotelId,
    userId,
    status,
    created,
  ]));
}
//create booking
const createBooking = (req, res) => {
  const bookingId = uuid();
  const created = now;
  const {
    name,
    nameNative,
    description,
    descriptionNative,
    photo,
    hotelId,
    userId,
    status,
  } = req.body;

  const createNotification = notification(
    bookingId,
    name,
    nameNative,
    description,
    descriptionNative,
    photo,
    hotelId,
    userId,
    status,
    created
  );

  //add booking to db
  pool.query(
    queries.addBooking,
    [
      bookingId,
      name,
      nameNative,
      description,
      descriptionNative,
      photo,
      hotelId,
      userId,
      status,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Student Created Successfully!");
    }
  );
};

//get hotel in province
async function getBookingStatus(status) {
  return (province = pool.query(queries.getBookingByStatus, [status]));
}

//get places hotels by province status
async function getBookingByStatus(req, res) {
  const pedding = "pedding";
  const approve = "approve";
  const done = "done";
  const bookingPedding = await getBookingStatus(pedding);
  const bookingApprove = await getBookingStatus(approve);
  const bookingDone = await getBookingStatus(done);
  const resultPedding = bookingPedding.rows;
  const resultApprove = bookingApprove.rows;
  const resultDone = bookingDone.rows;

  const results = [
    {
      key: "pedding",
      name: "Booking Pedding",
      booking: resultPedding,
    },
    {
      key: "approve",
      name: "Booking Approve",
      booking: resultApprove,
    },
    {
      key: "done",
      name: "Booking History",
      booking: resultDone,
    },
  ];

  res.send(results);
}

//get booking
const getBookings = (req, res) => {
  pool.query(queries.getBookings, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get booking by id
const getBookingById = (req, res) => {
  const bookingId = req.params.bookingId;
  pool.query(queries.getBookingById, [bookingId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update booking
const updateBooking = (req, res) => {
  const bookingId = req.params.bookingId;
  const updated = now;
  const {
    name,
    nameNative,
    description,
    descriptionNative,
    hotelId,
    userId,
    status,
  } = req.body;

  pool.query(queries.getBookingById, [bookingId], (error, results) => {
    if (error) throw error;
    const noBookingFound = !results.rows.length;
    if (noBookingFound) {
      res.send("Booking does not exist.");
    }

    //update booking
    pool.query(
      queries.updateBooking,
      [
        name,
        nameNative,
        description,
        descriptionNative,
        hotelId,
        userId,
        status,
        updated,
        bookingId,
      ],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Booking Updated Successfully!");
      }
    );
  });
};

//delete booking
const deleteBooking = (req, res) => {
  const bookingId = req.params.bookingId;

  pool.query(queries.getBookingById, [bookingId], (error, results) => {
    if (error) throw error;
    const noBookingFound = !results.rows.length;
    if (noBookingFound) {
      res.send("Booking does not exist.");
    }

    pool.query(queries.deleteBooking, [bookingId], (error, results) => {
      if (error) throw error;
      res.status(200).send("Booking deleted successfully.");
    });
  });
};
module.exports = {
  createBooking,
  getBookings,
  getBookingById,
  updateBooking,
  deleteBooking,
  getBookingByStatus,
};
