const pool = require("../config_db");
const queries = require("../lib/queries/queryPlaces");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

//create place
const createPlace = (req, res) => {
  const placeId = uuid();
  const created = now;
  const { name, nameNative, description, photo, provinceId, topTreding } =
    req.body;

  //add place to db
  pool.query(
    queries.addPlace,
    [
      placeId,
      name,
      nameNative,
      description,
      photo,
      provinceId,
      topTreding,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Place Created Successfully!");
    }
  );
};

//get places
const getPlaces = (req, res) => {
  pool.query(queries.getPlaces, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get place by id
const getPlaceById = (req, res) => {
  const placeId = req.params.placeId;
  pool.query(queries.getPlaceById, [placeId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//search place
const searchPlace = (req, res) => {
  const name = req.params.name;
  pool.query(queries.searchByName, [name], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update place
const updatePlace = (req, res) => {
  const placeId = req.params.placeId;
  const updated = now;
  const { name, nameNative, description, photo, provinceId, topTreding } =
    req.body;

  pool.query(queries.getPlaceById, [placeId], (error, results) => {
    if (error) throw error;
    const noPlaceFound = !results.rows.length;
    if (noPlaceFound) {
      res.send("Place does not exist.");
    }

    //update place
    pool.query(
      queries.updatePlace,
      [
        name,
        nameNative,
        description,
        photo,
        provinceId,
        topTreding,
        updated,
        placeId,
      ],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Place Updated Successfully!");
      }
    );
  });
};

//delete place
const deletePlace = (req, res) => {
  const placeId = req.params.placeId;

  pool.query(queries.getPlaceById, [placeId], (error, results) => {
    if (error) throw error;
    const noPlaceFound = !results.rows.length;
    if (noPlaceFound) {
      res.send("Place does not exist.");
    }

    pool.query(queries.deletePlace, [placeId], (error, results) => {
      if (error) throw error;
      res.status(200).send("Place deleted successfully.");
    });
  });
};

//get top treding
const getTopTredings = (req, res) => {
  const topTreding = "yes";
  pool.query(queries.getTopTredings, [topTreding], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//get recently place post
const getRecentlyPlace = (req, res) => {
  pool.query(queries.getPlaces, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate[0]);
  });
};

//get top place in province
const getTopPlacesInProvince = (req, res) => {
  const provinceId = req.params.provinceId;
  pool.query(queries.getPlacesInProvince, [provinceId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    const topPlacesInProvince = [];
    if (sortByDate.length > 0) {
      if (sortByDate.length < 4) {
        for (var i = 0; i < sortByDate.length; i++) {
          topPlacesInProvince.push(sortByDate[i]);
        }
      } else {
        topPlacesInProvince.push(sortByDate[0]);
        topPlacesInProvince.push(sortByDate[1]);
        topPlacesInProvince.push(sortByDate[2]);
        topPlacesInProvince.push(sortByDate[3]);
      }
    }
    res.status(200).json(topPlacesInProvince);
  });
};

//get places in province
const getPlacesInProvince = (req, res) => {
  const provinceId = req.params.provinceId;
  pool.query(queries.getPlacesInProvince, [provinceId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

module.exports = {
  createPlace,
  getPlaces,
  getPlaceById,
  searchPlace,
  updatePlace,
  deletePlace,
  getTopTredings,
  getRecentlyPlace,
  getTopPlacesInProvince,
  getPlacesInProvince,
};
