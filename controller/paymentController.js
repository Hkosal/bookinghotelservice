const pool = require("../config_db");
const queries = require("../lib/queries/queryPayments");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

//create payment
const createPayment = (req, res) => {
  const paymentId = uuid();
  const created = now;
  const {
    bookingId,
    userId,
    name,
    nameNative,
    description,
    descriptionNative,
    amount,
  } = req.body;

  //add payment to db
  pool.query(
    queries.addPayment,
    [
      paymentId,
      bookingId,
      userId,
      name,
      nameNative,
      description,
      descriptionNative,
      amount,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Payment Created Successfully!");
    }
  );
};

//get payments
const getPayments = (req, res) => {
  pool.query(queries.getPayments, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get payment by id
const getPaymentById = (req, res) => {
  const paymentId = req.params.paymentId;
  pool.query(queries.getPaymentById, [paymentId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update payment
const updatePayment = (req, res) => {
  const paymentId = req.params.paymentId;
  const updated = now;
  const {
    bookingId,
    userId,
    name,
    nameNative,
    description,
    descriptionNative,
    amount,
  } = req.body;

  pool.query(queries.getPaymentById, [paymentId], (error, results) => {
    if (error) throw error;
    const noPaymentFound = !results.rows.length;
    if (noPaymentFound) {
      res.send("Payment does not exist.");
    }

    //update payment
    pool.query(
      queries.updatePayment,
      [
        bookingId,
        userId,
        name,
        nameNative,
        description,
        descriptionNative,
        amount,
        updated,
        paymentId,
      ],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Payment Updated Successfully!");
      }
    );
  });
};

//delete payment
const deletePayment = (req, res) => {
  const paymentId = req.params.paymentId;

  pool.query(queries.getPaymentById, [paymentId], (error, results) => {
    if (error) throw error;
    const noPaymentFound = !results.rows.length;
    if (noPaymentFound) {
      res.send("Payment does not exist.");
    }

    pool.query(queries.deletePayment, [paymentId], (error, results) => {
      if (error) throw error;
      res.status(200).send("Payment deleted successfully.");
    });
  });
};
module.exports = {
  createPayment,
  getPayments,
  getPaymentById,
  updatePayment,
  deletePayment,
};
