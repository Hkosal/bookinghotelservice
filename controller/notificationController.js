const pool = require("../config_db");
const queries = require("../lib/queries/queryNotifications");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

//create notification
const createNotification = (req, res) => {
  const notificationId = uuid();
  const notificationDescription = "You booking have been successful.";
  const {
    bookingId,
    name,
    nameNative,
    description,
    descriptionNative,
    photo,
    hotelId,
    userId,
    status,
    created,
  } = req.body;
  if (!created) {
    created = now;
  }

  //add notification to db
  pool.query(
    queries.addNotification,
    [
      notificationId,
      notificationDescription,
      bookingId,
      name,
      nameNative,
      description,
      descriptionNative,
      photo,
      hotelId,
      userId,
      status,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Notification Created Successfully!");
    }
  );
};

//get notifications
const getNotifications = (req, res) => {
  pool.query(queries.getNotifications, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//delete notification
const deleteNotification = (req, res) => {
  const notificationId = req.params.notificationId;

  pool.query(
    queries.getNotificationById,
    [notificationId],
    (error, results) => {
      if (error) throw error;
      const noNotificationFound = !results.rows.length;
      if (noNotificationFound) {
        res.send("Notification does not exist.");
      }

      pool.query(
        queries.deleteNotification,
        [notificationId],
        (error, results) => {
          if (error) throw error;
          res.status(200).send("Notification deleted successfully.");
        }
      );
    }
  );
};

//get notification by user
const getNotificationByUser = (req, res) => {
  const userId = req.params.userId;
  pool.query(queries.notificationByUser, [userId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get notification by id
const getNotificationById = (req, res) => {
  const notificationId = req.params.notificationId;
  pool.query(
    queries.getNotificationById,
    [notificationId],
    (error, results) => {
      if (error) throw error;
      const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
      res.status(200).json(sortByDate);
    }
  );
};

module.exports = {
  createNotification,
  getNotifications,
  getNotificationById,
  deleteNotification,
  getNotificationByUser,
};
