const pool = require("../config_db");
const queries = require("../lib/queries/queryFavoritePlaces");
const queryPlace = require("../lib/queries/queryPlaces");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

// get place
async function getPlaceById(placeId) {
  return (place = pool.query(queryPlace.getPlaceById, [placeId]));
}
//create favorite place
async function createFavoritePlace(req, res) {
  const favoritePlaceId = uuid();
  const created = now;
  const { userId, placeId } = req.body;
  const place = await getPlaceById(placeId);
  const resultPlace = place.rows[0];
  const { name, namenative, description, photo, provinceid, toptreding } =
    resultPlace;

  //add favorite place to db
  pool.query(
    queries.addFavoritePlace,
    [
      favoritePlaceId,
      userId,
      placeId,
      name,
      namenative,
      description,
      photo,
      provinceid,
      toptreding,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Favorite Place Created Successfully!");
    }
  );
}

//get favorite places
const getFavoritePlaces = (req, res) => {
  pool.query(queries.getFavoritePlaces, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get favorite places by userId
const getFavoritePlaceByUserId = (req, res) => {
  const userId = req.params.userId;
  pool.query(queries.getFavoritePlacesByUser, [userId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get favorite place by id
const getFavoritePlaceById = (req, res) => {
  const favoritePlaceId = req.params.favoritePlaceId;
  pool.query(
    queries.getFavoritePlaceById,
    [favoritePlaceId],
    (error, results) => {
      if (error) throw error;
      res.status(200).json(results.rows);
    }
  );
};

//update favorite place
const updateFavoritePlace = (req, res) => {
  const favoritePlaceId = req.params.favoritePlaceId;
  const { userId, placeId } = req.body;

  pool.query(
    queries.getFavoritePlaceById,
    [favoritePlaceId],
    (error, results) => {
      if (error) throw error;
      const noFavoritePlaceFound = !results.rows.length;
      if (noFavoritePlaceFound) {
        res.send("Favorite Place does not exist.");
      }

      //update favorite place
      pool.query(
        queries.updateFavoritePlace,
        [userId, placeId, favoritePlaceId],
        (error, results) => {
          if (error) throw error;
          res.status(200).send("Favorite Place Updated Successfully!");
        }
      );
    }
  );
};

//delete favorite place
const deleteFavoritePlace = (req, res) => {
  const favoritePlaceId = req.params.favoritePlaceId;

  pool.query(
    queries.getFavoritePlaceById,
    [favoritePlaceId],
    (error, results) => {
      if (error) throw error;
      const noFavoritePlaceFound = !results.rows.length;
      if (noFavoritePlaceFound) {
        res.send("Favorite Place does not exist.");
      }

      pool.query(
        queries.deleteFavoritePlace,
        [favoritePlaceId],
        (error, results) => {
          if (error) throw error;
          res.status(200).send("Favorite Place deleted successfully.");
        }
      );
    }
  );
};
module.exports = {
  createFavoritePlace,
  getFavoritePlaces,
  getFavoritePlaceById,
  updateFavoritePlace,
  deleteFavoritePlace,
  getFavoritePlaceByUserId,
};
