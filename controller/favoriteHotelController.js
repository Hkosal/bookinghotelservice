const pool = require("../config_db");
const queries = require("../lib/queries/queryFavoriteHotels");
const queryHotel = require("../lib/queries/queryHotels");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

// get hotel
async function getHotelById(hotelId) {
  return (hotel = pool.query(queryHotel.getHotelById, [hotelId]));
}
//create favorite hotel
async function createFavoriteHotel(req, res) {
  const favoriteHotelId = uuid();
  const created = now;
  const { userId, hotelId } = req.body;
  const hotel = await getHotelById(hotelId);
  const resultHotel = hotel.rows[0];
  const {
    name,
    namenative,
    description,
    descriptionnative,
    photo,
    provinceid,
    placeid,
  } = resultHotel;

  // add favorite hotel to db
  pool.query(
    queries.addFavoriteHotel,
    [
      favoriteHotelId,
      userId,
      hotelId,
      name,
      namenative,
      description,
      descriptionnative,
      photo,
      provinceid,
      placeid,
      created,
    ],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Favorite Hotel Created Successfully!");
    }
  );
}

//get favorite hotels
const getFavoriteHotels = (req, res) => {
  pool.query(queries.getFavoriteHotels, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get favorite hotel by id
const getFavoriteHotelById = (req, res) => {
  const favoriteHotelId = req.params.favoriteHotelId;
  pool.query(
    queries.getFavoriteHotelById,
    [favoriteHotelId],
    (error, results) => {
      if (error) throw error;
      res.status(200).json(results.rows);
    }
  );
};

//get favorite hotel by userId
const getFavoriteHotelByUserId = (req, res) => {
  const userId = req.params.userId;
  pool.query(queries.getFavoriteHotelsByUser, [userId], (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//update favorite hotel
const updateFavoriteHotel = (req, res) => {
  const favoriteHotelId = req.params.favoriteHotelId;
  const { userId, hotelId } = req.body;

  pool.query(
    queries.getFavoriteHotelById,
    [favoriteHotelId],
    (error, results) => {
      if (error) throw error;
      const noFavoriteHotelFound = !results.rows.length;
      if (noFavoriteHotelFound) {
        res.send("Favorite Hotel does not exist.");
      }

      //update favorite hotel
      pool.query(
        queries.updateFavoriteHotel,
        [userId, hotelId, favoriteHotelId],
        (error, results) => {
          if (error) throw error;
          res.status(200).send("Favorite Hotel Updated Successfully!");
        }
      );
    }
  );
};

//delete favorite hotel
const deleteFavoriteHotel = (req, res) => {
  const favoriteHotelId = req.params.favoriteHotelId;

  pool.query(
    queries.getFavoriteHotelById,
    [favoriteHotelId],
    (error, results) => {
      if (error) throw error;
      const noFavoriteHotelFound = !results.rows.length;
      if (noFavoriteHotelFound) {
        res.send("Favorite Hotel does not exist.");
      }

      pool.query(
        queries.deleteFavoriteHotel,
        [favoriteHotelId],
        (error, results) => {
          if (error) throw error;
          res.status(200).send("Favorite Hotel deleted successfully.");
        }
      );
    }
  );
};
module.exports = {
  createFavoriteHotel,
  getFavoriteHotels,
  getFavoriteHotelById,
  updateFavoriteHotel,
  deleteFavoriteHotel,
  getFavoriteHotelByUserId,
};
