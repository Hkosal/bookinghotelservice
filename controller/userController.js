const pool = require("../config_db");
const queries = require("../lib/queries/queryUsers");
const { v4: uuid } = require("uuid");
const now = new Date().toISOString().slice(0, 10);

//create user
const createUser = (req, res) => {
  const userId = uuid();
  const created = now;
  const {
    firstname,
    lastname,
    photo,
    email,
    phone,
    password,
    address,
    gender,
  } = req.body;
  //check if email exists
  pool.query(queries.checkEmailExists, [email], (error, results) => {
    if (results.rows.length) {
      res.send("Email already exists.");
    }

    //add user to db
    pool.query(
      queries.addUser,
      [
        userId,
        firstname,
        lastname,
        photo,
        email,
        phone,
        password,
        address,
        created,
        gender,
      ],
      (error, results) => {
        if (error) throw error;
        res.status(201).send("User Created Successfully!");
      }
    );
  });
};

//get users
const getUsers = (req, res) => {
  pool.query(queries.getUsers, (error, results) => {
    if (error) throw error;
    const sortByDate = results.rows.sort((a, b) => a.dob - b.dob);
    res.status(200).json(sortByDate);
  });
};

//get user by id
const getUserById = (req, res) => {
  const userId = req.params.userId;
  pool.query(queries.getUserById, [userId], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//update user
const updateUser = (req, res) => {
  const userId = req.params.userId;
  const updated = now;
  const { firstname, lastname, email, phone, address, gender, photo } =
    req.body;

  pool.query(queries.getUserById, [userId], (error, results) => {
    if (error) throw error;
    const noUserFound = !results.rows.length;
    if (noUserFound) {
      res.send("User does not exist.");
    }

    //update user
    pool.query(
      queries.updateUser,
      [
        firstname,
        lastname,
        email,
        phone,
        address,
        updated,
        gender,
        photo,
        userId,
      ],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("User Updated Successfully!");
      }
    );
  });
};

//delete user
const deleteUser = (req, res) => {
  const userId = req.params.userId;

  pool.query(queries.getUserById, [userId], (error, results) => {
    if (error) throw error;
    const noUserFound = !results.rows.length;
    if (noUserFound) {
      res.send("User does not exist.");
    }

    pool.query(queries.deleteUser, [userId], (error, results) => {
      if (error) throw error;
      res.status(200).send("User deleted successfully.");
    });
  });
};
module.exports = {
  createUser,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
};
