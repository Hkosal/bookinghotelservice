const { Router } = require("express");
const router = Router();
const bookings = require("./api/bookings");
const comments = require("./api/comments");
const favoriteHotels = require("./api/favoriteHotels");
const favoritePlaces = require("./api/favoritePlaces");
const hotels = require("./api/hotels");
const payments = require("./api/payments");
const places = require("./api/places");
const provinces = require("./api/provinces");
const users = require("./api/users");
const notifications = require("./api/notifications");

router.use("/bookings", bookings);
router.use("/comments", comments);
router.use("/favorite_hotels", favoriteHotels);
router.use("/favorite_places", favoritePlaces);
router.use("/hotels", hotels);
router.use("/payments", payments);
router.use("/places", places);
router.use("/provinces", provinces);
router.use("/users", users);
router.use("/notifications", notifications);

module.exports = router;
