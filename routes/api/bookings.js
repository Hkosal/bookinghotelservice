const { Router } = require("express");
const bookingController = require("../../controller/bookingController");

const router = Router();

router.post("/", bookingController.createBooking);
router.get("/", bookingController.getBookings);
router.get("/booking/:bookingId", bookingController.getBookingById);
router.put("/booking/:bookingId", bookingController.updateBooking);
router.delete("/booking/:bookingId", bookingController.deleteBooking);
router.get("/bookingbystatus", bookingController.getBookingByStatus);

module.exports = router;
