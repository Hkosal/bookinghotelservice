const { Router } = require("express");
const paymentController = require("../../controller/paymentController");

const router = Router();

router.post("/", paymentController.createPayment);
router.get("/", paymentController.getPayments);
router.get("/payment/:paymentId", paymentController.getPaymentById);
router.put("/payment/:paymentId", paymentController.updatePayment);
router.delete("/payment/:paymentId", paymentController.deletePayment);

module.exports = router;
