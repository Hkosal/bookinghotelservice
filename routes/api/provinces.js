const { Router } = require("express");
const provinceController = require("../../controller/provinceController");

const router = Router();

router.post("/", provinceController.createProvince);
router.get("/", provinceController.getProvinces);
router.get("/province/:provinceId", provinceController.getProvinceById);
router.put("/province/:provinceId", provinceController.updateProvince);
router.delete("/province/:provinceId", provinceController.deleteProvince);
router.get("/provinceplaceshotels", provinceController.getProvincePlacesHotels);
router.get(
  "/placeshotelsbyprovince/:provinceId",
  provinceController.getPlacesHotelsByProvince
);

module.exports = router;
