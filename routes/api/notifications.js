const { Router } = require("express");
const notificationController = require("../../controller/notificationController");

const router = Router();

router.post("/", notificationController.createNotification);
router.get("/", notificationController.getNotifications);
router.get(
  "/notification/:notificationId",
  notificationController.getNotificationById
);
router.delete(
  "/notification/:notificationId",
  notificationController.deleteNotification
);
router.get(
  "/notification_by_user/:userId",
  notificationController.getNotificationByUser
);

module.exports = router;
