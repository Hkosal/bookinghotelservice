const { Router } = require("express");
const userController = require("../../controller/userController");

const router = Router();

router.post("/", userController.createUser);
router.get("/", userController.getUsers);
router.get("/user/:userId", userController.getUserById);
router.put("/user/:userId", userController.updateUser);
router.delete("/user/:userId", userController.deleteUser);

module.exports = router;
