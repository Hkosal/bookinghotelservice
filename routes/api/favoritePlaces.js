const { Router } = require("express");
const favoritePlaceController = require("../../controller/favoritePlaceController");

const router = Router();

router.post("/", favoritePlaceController.createFavoritePlace);
router.get("/", favoritePlaceController.getFavoritePlaces);
router.get(
  "/favorite_place/:favoritePlaceId",
  favoritePlaceController.getFavoritePlaceById
);
router.get("/:userId", favoritePlaceController.getFavoritePlaceByUserId);
router.put(
  "/favorite_place/:favoritePlaceId",
  favoritePlaceController.updateFavoritePlace
);
router.delete(
  "/favorite_place/:favoritePlaceId",
  favoritePlaceController.deleteFavoritePlace
);

module.exports = router;
