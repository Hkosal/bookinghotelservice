const { Router } = require("express");
const commentController = require("../../controller/commentController");

const router = Router();

router.post("/", commentController.createComment);
router.get("/", commentController.getComments);
router.get("/comment/:commentId", commentController.getCommentById);
router.put("/comment/:commentId", commentController.updateComment);
router.delete("/comment/:commentId", commentController.deleteComment);

module.exports = router;
