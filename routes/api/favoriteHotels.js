const { Router } = require("express");
const favoriteHotelController = require("../../controller/favoriteHotelController");

const router = Router();

router.post("/", favoriteHotelController.createFavoriteHotel);
router.get("/", favoriteHotelController.getFavoriteHotels);
router.get(
  "/favorite_hotel/:favoriteHotelId",
  favoriteHotelController.getFavoriteHotelById
);
router.get("/:userId", favoriteHotelController.getFavoriteHotelByUserId);
router.put(
  "/favorite_hotel/:favoriteHotelId",
  favoriteHotelController.updateFavoriteHotel
);
router.delete(
  "/favorite_hotel/:favoriteHotelId",
  favoriteHotelController.deleteFavoriteHotel
);

module.exports = router;
