const { Router } = require("express");
const hotelController = require("../../controller/hotelController");

const router = Router();

router.post("/", hotelController.createHotel);
router.get("/", hotelController.getHotels);
router.get("/hotel/:hotelId", hotelController.getHotelById);
router.put("/hotel/:hotelId", hotelController.updateHotel);
router.delete("/hotel/:hotelId", hotelController.deleteHotel);
router.get(
  "/tophotelsinprovince/:provinceId",
  hotelController.getTopHotelsInProvince
);
router.get(
  "/hotelsinprovince/:provinceId",
  hotelController.getHotelsInProvince
);
router.get("/tophotelsinplace/:placeId", hotelController.getTopHotelsInPlace);
router.get("/hotelsinplace/:placeId", hotelController.getHotelsInPlace);

module.exports = router;
