const { Router } = require("express");
const placeController = require("../../controller/placeController");

const router = Router();

router.post("/", placeController.createPlace);
router.get("/", placeController.getPlaces);
router.get("/place/:placeId", placeController.getPlaceById);
router.put("/place/:placeId", placeController.updatePlace);
router.delete("/place/:placeId", placeController.deletePlace);
router.get("/toptredings", placeController.getTopTredings);
router.get("/recentlyplace", placeController.getRecentlyPlace);
router.get(
  "/topplacesinprovince/:provinceId",
  placeController.getTopPlacesInProvince
);
router.get(
  "/placesinprovince/:provinceId",
  placeController.getPlacesInProvince
);
router.get("/search/:name", placeController.searchPlace);

module.exports = router;
