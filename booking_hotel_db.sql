--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bookings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bookings (
    bookingid character varying(255) NOT NULL,
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    descriptionnative character varying(255),
    hotelid character varying(255),
    userid character varying(255),
    status character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone,
    photo json
);


ALTER TABLE public.bookings OWNER TO postgres;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comments (
    commentid character varying(255) NOT NULL,
    userid character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    phone character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone
);


ALTER TABLE public.comments OWNER TO postgres;

--
-- Name: favorite_hotels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.favorite_hotels (
    favoritehotelid character varying(255) NOT NULL,
    userid character varying(255),
    hotelid character varying(255),
    created timestamp without time zone,
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    descriptionnative character varying(255),
    photo json,
    provinceid character varying(255),
    placeid character varying(255)
);


ALTER TABLE public.favorite_hotels OWNER TO postgres;

--
-- Name: favorite_places; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.favorite_places (
    favoriteplaceid character varying(255) NOT NULL,
    userid character varying(255),
    placeid character varying(255),
    created timestamp without time zone,
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    provinceid character varying(255),
    toptreding character varying(255),
    photo json
);


ALTER TABLE public.favorite_places OWNER TO postgres;

--
-- Name: hotels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hotels (
    hotelid character varying(255) NOT NULL,
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    descriptionnative character varying(255),
    photo json,
    placeid character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone,
    provinceid character varying(255)
);


ALTER TABLE public.hotels OWNER TO postgres;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notifications (
    notificationid character varying(255) NOT NULL,
    notificationdescription character varying(255),
    bookingid character varying(255),
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    descriptionnative character varying(255),
    hotelid character varying(255),
    userid character varying(255),
    status character varying(255),
    created character varying(255),
    photo json
);


ALTER TABLE public.notifications OWNER TO postgres;

--
-- Name: payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments (
    paymentid character varying(255) NOT NULL,
    bookingid character varying(255),
    userid character varying(255),
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    descriptionnative character varying(255),
    amount character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone
);


ALTER TABLE public.payments OWNER TO postgres;

--
-- Name: places; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.places (
    placeid character varying(255) NOT NULL,
    name character varying(255),
    namenative character varying(255),
    description character varying(255),
    photo json,
    provinceid character varying(255),
    toptreding character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone
);


ALTER TABLE public.places OWNER TO postgres;

--
-- Name: provinces; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provinces (
    provinceid character varying(255) NOT NULL,
    name character varying(255),
    namenative character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone
);


ALTER TABLE public.provinces OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    userid character varying(255) NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    photo json,
    email character varying(255),
    phone character varying(255),
    password character varying(255),
    address character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone,
    deleted timestamp without time zone,
    gender character varying(255)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: bookings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bookings (bookingid, name, namenative, description, descriptionnative, hotelid, userid, status, created, updated, photo) FROM stdin;
9cd6c691-2133-4055-9d6e-f5a5f9b56ce7	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	pedding	2021-08-16 00:00:00	\N	\N
75987762-a613-408a-b49e-7aad7731b7dc	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	pedding	2021-08-18 00:00:00	\N	\N
3db054ba-5a55-48d5-85a4-2b976c4fed94	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	approve	2021-08-18 00:00:00	\N	\N
72ab3797-d959-4e65-ad59-45aa4963c6cc	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	approve	2021-08-18 00:00:00	\N	\N
168b501b-0b5b-4cba-b6d7-771e3f699de3	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	done	2021-08-18 00:00:00	\N	\N
4ff0c9cb-e18d-47da-99a5-4b3669c6c745	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	done	2021-08-18 00:00:00	\N	\N
a21ae1c6-333b-4064-a265-00c50d81e411	Hotel	Hotel Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	123	123	done	2021-08-18 00:00:00	\N	\N
1c5b4c2e-105d-4663-bcf4-246d7bf80cb0	Hotel Name	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29 00:00:00	\N	\N
2367d23f-a6d1-4733-bbc3-b51c029c9b93	Hotel Name	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29 00:00:00	\N	\N
8bf31d8b-01e4-43ac-90eb-574c5b456854	Hotel Name	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29 00:00:00	\N	\N
65db44fd-a8a7-400f-bb06-e32126ca98b0	Hotel Name	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29 00:00:00	\N	\N
98460f8a-5d17-46ae-b4b1-396fc35cb2a1	Hotel Name	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29 00:00:00	\N	\N
4f22ea07-52bd-43d3-9eac-fb6995389d4a	Hotel Name Kosal	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29 00:00:00	\N	{"uri":"photo url"}
2899a00e-b32e-4c02-88aa-d1326dd81252	Baitong Hotel	សណ្តាគារបាយតុង	At Baitong Hotel we take great care to provide you a great hotel experience which includes the cleanliness of our hotel. To continue to provide an environment which is as safe as possible for you, and our team, 	we have taken extra steps to clean and sanitize our hotel more frequently. Every day we are following strict cleaning, quality control and hygiene procedures in all hotel areas.	0a375551-279e-45f7-931d-9b784132708c	cd925b7d-5136-4cd6-8708-9023fc6f97cc	pedding	2021-08-31 00:00:00	\N	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/9d/84/ae/baitong-hotel-resort.jpg?w=900&h=-1&s=1"}
5855a3b1-ea3a-4820-9de6-d2a42e04d421	Baitong Hotel	សណ្តាគារបាយតុង	At Baitong Hotel we take great care to provide you a great hotel experience which includes the cleanliness of our hotel. To continue to provide an environment which is as safe as possible for you, and our team, 	we have taken extra steps to clean and sanitize our hotel more frequently. Every day we are following strict cleaning, quality control and hygiene procedures in all hotel areas.	0a375551-279e-45f7-931d-9b784132708c	cd925b7d-5136-4cd6-8708-9023fc6f97cc	pedding	2021-08-31 00:00:00	\N	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/9d/84/ae/baitong-hotel-resort.jpg?w=900&h=-1&s=1"}
\.


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comments (commentid, userid, firstname, lastname, phone, created, updated) FROM stdin;
9238fecf-a6ca-49b0-8fed-6858bfa16deb	book room	book room	book 1 room that have 2 bed	+855969604049	2021-08-16 00:00:00	\N
\.


--
-- Data for Name: favorite_hotels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.favorite_hotels (favoritehotelid, userid, hotelid, created, name, namenative, description, descriptionnative, photo, provinceid, placeid) FROM stdin;
451c39d7-0747-4e78-a212-bc5caff699b5	cd925b7d-5136-4cd6-8708-9023fc6f97cc	0a375551-279e-45f7-931d-9b784132708c	2021-08-22 00:00:00	Baitong Hotel	សណ្តាគារបាយតុង	At Baitong Hotel we take great care to provide you a great hotel experience which includes the cleanliness of our hotel. To continue to provide an environment which is as safe as possible for you, and our team, 	we have taken extra steps to clean and sanitize our hotel more frequently. Every day we are following strict cleaning, quality control and hygiene procedures in all hotel areas.	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/9d/84/ae/baitong-hotel-resort.jpg?w=900&h=-1&s=1"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	c048523c-ea0b-4ff6-b62a-d06fdcb2a925
d006ba14-d481-47d8-a248-fc4b8951d128	cd925b7d-5136-4cd6-8708-9023fc6f97cc	80f8371c-1e3c-44ef-b695-3a0d7f3ae0aa	2021-08-22 00:00:00	Palace gate hotel	សណ្តាគារផាលេសគេត	All linens sanitized in high-temperature wash​ Face masks required for staff in public areas Hand sanitizer available to guests & staff Regularly sanitized high-traffic areas Staff required to regularly wash hands​	Daily temperature and symptom checks for staff Doctors available 24/7 Individually wrapped toiletries	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/14/11/b9/7d/palace-gate-hotel-resort.jpg?w=600&h=300&s=1"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	c048523c-ea0b-4ff6-b62a-d06fdcb2a925
3c6b7cf9-ab4a-406f-a82d-2e0613f0197e	cd925b7d-5136-4cd6-8708-9023fc6f97cc	085c4e31-1324-4de1-8e91-037914ac6d60	2021-08-22 00:00:00	The Bale hotel	សណ្តាគារបាលេ	Our hearts are with those who have lost their loved ones while at the same time, we join our hands in prayer for the affected families and patients.	Two large swimming pools and private plunge pools contribute to the oasis feel of a place where each and every one of its accommodations keeps its own character and atmosphere.	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/11/42/80/0c/the-bale-phnom-penh-resort.jpg?w=900&h=-1&s=1"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	c048523c-ea0b-4ff6-b62a-d06fdcb2a925
c223b038-c0e9-47c9-9656-165b0623db95	cd925b7d-5136-4cd6-8708-9023fc6f97cc	da6fc2c5-0ca2-43c5-af4c-5df4513c0723	2021-08-22 00:00:00	Saem Siemreap Hotel	សណ្តាគារសៀម សៀមរាប	Check-in is from 02:00 PM, and check-out is until 12:00 PM. You may request early check-in or late check-out during booking, subject to availability. Guests checking in or out before or after the designated periods may be charged an additional fee.	The hotel offers luggage storage to guests for before check-in and after check-out. The front desk is always open, day or night.	{"uri":"https://pix8.agoda.net/hotelImages/408/408121/408121_15070709250031778874.jpg"}	5a8c8afa-5a09-43fb-8eb5-7832c102d184	5aca822f-6de4-4f84-b1f9-740984b8b25e
\.


--
-- Data for Name: favorite_places; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.favorite_places (favoriteplaceid, userid, placeid, created, name, namenative, description, provinceid, toptreding, photo) FROM stdin;
27130ce5-efa1-4cec-adf2-6b1477306992	cd925b7d-5136-4cd6-8708-9023fc6f97cc	5aca822f-6de4-4f84-b1f9-740984b8b25e	2021-08-22 00:00:00	Wander Pub Street	ផ្សាររាត្រីក្រុងសៀមរាប	With so much culture, history, and ancient architecture to take in during the day, you might just want to let off some mindless steam at night. For a relaxed evening of western food and a wild night of Angkor beers on tap, head to Pub Street.	5a8c8afa-5a09-43fb-8eb5-7832c102d184	yes	{"uri":"https://cdn.thecrazytourist.com/wp-content/uploads/2018/09/ccimage-shutterstock_635149646.jpg"}
42cf2b33-2c04-4281-b6f2-a2c3dc703a60	cd925b7d-5136-4cd6-8708-9023fc6f97cc	9b222cd4-0831-40ff-a091-0cd4037f50fb	2021-08-22 00:00:00	Angkor Wat Temple	ប្រាសាទអង្គរវត្ត	Angkor Wat lies 5.5 kilometres (3+1⁄2 mi) north of the modern town of Siem Reap, and a short distance south and slightly east of the previous capital, which was centred at Baphuon.	5a8c8afa-5a09-43fb-8eb5-7832c102d184	yes	{"uri":"https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ankor_Wat_temple.jpg/1200px-Ankor_Wat_temple.jpg"}
0ee4b8a1-43d0-4547-88bc-7c65e5df46c9	cd925b7d-5136-4cd6-8708-9023fc6f97cc	c048523c-ea0b-4ff6-b62a-d06fdcb2a925	2021-08-22 00:00:00	Royal Palace	ព្រះរាជវ៉ាំង	The highlight of Phnom Penh is the beautiful Royal Palace - the seat and residence of Cambodia's royal family since the 1860s. The spired-roof pavilions of the complex are an excellent example of classic Khmer architecture.	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	yes	{"uri":"https://www.planetware.com/wpimages/2020/07/cambodia-phnom-penh-top-attractions-royal-palace.jpg"}
905c57d7-17e3-44b1-a7c8-02642f87c62d	cd925b7d-5136-4cd6-8708-9023fc6f97cc	c4ff14e0-308d-42f6-9a14-bda9dc145441	2021-08-22 00:00:00	River Boat Cruises	ជិះទូកមើលទីក្រុងភ្នំពេញ	Phnom Penh is a riverine town, and one of the most relaxing ways to go sightseeing in the city is to take to the water. There are regular sunset cruise tourist boat departures from the riverfront between 5pm and 7.30pm,	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	yes	{"uri":"https://www.planetware.com/photos-large/CAM/cambodia-phnom-penh-boat-tour-on-mekong-river.jpg"}
05dd925e-faed-4fcf-a6df-67016f174e13	cd925b7d-5136-4cd6-8708-9023fc6f97cc	6ed8c0a7-bd3e-48c4-b3d8-20bd581ae9b8	2021-08-31 00:00:00	Kulen Moutain	ភ្នំគូលេន	Talk to a tuk tuk driver to help you find a guide for the Kulen Nature Trails. These rainforest trails offer hidden gems in the rainforest such as ancient temples, respected monasteries, and rushing waterfalls.	5a8c8afa-5a09-43fb-8eb5-7832c102d184	yes	{"uri":"https://cdn.thecrazytourist.com/wp-content/uploads/2018/09/ccimage-shutterstock_689409427.jpg"}
\.


--
-- Data for Name: hotels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hotels (hotelid, name, namenative, description, descriptionnative, photo, placeid, created, updated, provinceid) FROM stdin;
0a375551-279e-45f7-931d-9b784132708c	Baitong Hotel	សណ្តាគារបាយតុង	At Baitong Hotel we take great care to provide you a great hotel experience which includes the cleanliness of our hotel. To continue to provide an environment which is as safe as possible for you, and our team, 	we have taken extra steps to clean and sanitize our hotel more frequently. Every day we are following strict cleaning, quality control and hygiene procedures in all hotel areas.	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/9d/84/ae/baitong-hotel-resort.jpg?w=900&h=-1&s=1"}	c048523c-ea0b-4ff6-b62a-d06fdcb2a925	2021-08-21 00:00:00	\N	2f3be322-f07e-4ea5-badf-d6b89fd4eeba
80f8371c-1e3c-44ef-b695-3a0d7f3ae0aa	Palace gate hotel	សណ្តាគារផាលេសគេត	All linens sanitized in high-temperature wash​ Face masks required for staff in public areas Hand sanitizer available to guests & staff Regularly sanitized high-traffic areas Staff required to regularly wash hands​	Daily temperature and symptom checks for staff Doctors available 24/7 Individually wrapped toiletries	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/14/11/b9/7d/palace-gate-hotel-resort.jpg?w=600&h=300&s=1"}	c048523c-ea0b-4ff6-b62a-d06fdcb2a925	2021-08-21 00:00:00	\N	2f3be322-f07e-4ea5-badf-d6b89fd4eeba
81f251e2-e3e2-448b-99b1-141327b4d74f	Pavilion hotel	សណ្តាគារផាវិលាន	A few steps from the Royal Palace and Wat Botum (Royal Pagoda), a collection of four period buildings, including what presumably was Queen Kossamak's private residence. There is a sense of timeless tranquility among exuberant flora.​	Two large swimming pools and private plunge pools contribute to the oasis feel of a place where each and every one of its accommodations keeps its own character and atmosphere.	{"uri":"https://maads.asia/imager/libs/images/pavilion/pages/pavilion/2151/pavilion-img-slider-home_0e0db31104dd16366ee70e919dde56fd.jpg"}	c048523c-ea0b-4ff6-b62a-d06fdcb2a925	2021-08-21 00:00:00	\N	2f3be322-f07e-4ea5-badf-d6b89fd4eeba
b8034d59-6782-47c8-85ab-2bba311804dc	Himawari hotel apartments	សណ្តាគារហ៊ីម៉ាវ៉ារី	Perched on the idyllic setting of Tonle Sap River, banks of the Chaktomuk River, be immersed by the essence of serenity at Himawari Hotel Apartments; a five-star establishment.	Himawari Hotel Apartments was first launched as Micasa Hotel Apartments on 20 June 2001 by His Excellency Samdech HUN SEN, Prime Minister of The Royal Government of Cambodia.	{"uri":"http://www.himawarihotel.com/modules/mod_image_show_gk4/cache/slideshow.slideshow01gk-is-559.jpg"}	c048523c-ea0b-4ff6-b62a-d06fdcb2a925	2021-08-21 00:00:00	\N	2f3be322-f07e-4ea5-badf-d6b89fd4eeba
085c4e31-1324-4de1-8e91-037914ac6d60	The Bale hotel	សណ្តាគារបាលេ	Our hearts are with those who have lost their loved ones while at the same time, we join our hands in prayer for the affected families and patients.	Two large swimming pools and private plunge pools contribute to the oasis feel of a place where each and every one of its accommodations keeps its own character and atmosphere.	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/11/42/80/0c/the-bale-phnom-penh-resort.jpg?w=900&h=-1&s=1"}	c048523c-ea0b-4ff6-b62a-d06fdcb2a925	2021-08-21 00:00:00	\N	2f3be322-f07e-4ea5-badf-d6b89fd4eeba
d0484631-e53f-4eeb-9d87-5a8df415649c	ANGKOR HOTEL	សណ្តាគារអង្គរ	Angkor Hotel has a large swimming pool available for that refreshing swim during the hot day or a welcome dip at day's end after your trekking around the famous Angkor Temples. Refreshments can be served at poolside	Enjoy a memorable stay in charming Khmer-inspired ambience. Select from two distinctive, all-affordable, all-comfortable room types with full facilities. Our Deluxe rooms and Junior Suite rooms are well-designed,	{"uri":"https://www.angkorhotel.net/userfiles/11.jpg"}	5aca822f-6de4-4f84-b1f9-740984b8b25e	2021-08-21 00:00:00	\N	5a8c8afa-5a09-43fb-8eb5-7832c102d184
d2cc95ee-f52c-40ca-9c49-ffdb7b81990b	Allson Angkor Hotel	សណ្តាគារអាលសុនអង្គរ	With a décor and an architectural design inspired by the traditional motifs of Khmer culture, the three-star Allson Angkor Hotel Siem Reap is in a world of its own.	Step into the lobby, and you are immediately transported to a setting reminiscent of the Angkorian era, blended with the comforts of a modern hotel.	{"uri":"http://static.asiawebdirect.com/m/bangkok/hotels/visit-mekong-com/allson-angkor/hotelBanner/home.jpg"}	5aca822f-6de4-4f84-b1f9-740984b8b25e	2021-08-21 00:00:00	\N	5a8c8afa-5a09-43fb-8eb5-7832c102d184
6abb996f-5dba-4f1d-9179-0be84934b3ad	Empress Angkor Resort	សណ្តាគារអិមផ្រេស	Grace with warm hospitality and adorned with traditional Cambodia beauty, the Empress Angkor Resort & Spa welcome you to relive the grandeur of the past from the present.	The Empress Angkor Resort & Spa is a fine blend of traditional Cambodian design, cluture and modern touches, making it a place of tranquility and comfort to rejuvenate your body and soul before and after you set out to explore	{"uri":"https://www.empressangkor.com/userfiles/thumbs/31.jpg"}	5aca822f-6de4-4f84-b1f9-740984b8b25e	2021-08-21 00:00:00	\N	5a8c8afa-5a09-43fb-8eb5-7832c102d184
da6fc2c5-0ca2-43c5-af4c-5df4513c0723	Saem Siemreap Hotel	សណ្តាគារសៀម សៀមរាប	Check-in is from 02:00 PM, and check-out is until 12:00 PM. You may request early check-in or late check-out during booking, subject to availability. Guests checking in or out before or after the designated periods may be charged an additional fee.	The hotel offers luggage storage to guests for before check-in and after check-out. The front desk is always open, day or night.	{"uri":"https://pix8.agoda.net/hotelImages/408/408121/408121_15070709250031778874.jpg"}	5aca822f-6de4-4f84-b1f9-740984b8b25e	2021-08-21 00:00:00	\N	5a8c8afa-5a09-43fb-8eb5-7832c102d184
6b58e38c-9eb0-4296-9410-b27091e22fb1	TARA ANGKOR Hotel	សណ្តាគារតារា អង្គរ	Experience all that Siem Reap has to offer in our stylish 4 star International hotel. The Tara Angkor Hotel is an ideal base to explore Siem Reap and the magnificent temples steeped in legend and history.	Address:Vithei Charles de Gaulle (Road to Angkor Wat), Siem Reap, Kingdom of Cambodia	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1b/3a/70/f9/exterior.jpg?w=900&h=-1&s=1"}	5aca822f-6de4-4f84-b1f9-740984b8b25e	2021-08-22 00:00:00	\N	5a8c8afa-5a09-43fb-8eb5-7832c102d184
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notifications (notificationid, notificationdescription, bookingid, name, namenative, description, descriptionnative, hotelid, userid, status, created, photo) FROM stdin;
11d4a9a9-4de9-4949-9387-ed75411eabcd	You booking have been successful.	98460f8a-5d17-46ae-b4b1-396fc35cb2a1	Hotel Name	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29	\N
85b4a88f-d09f-41da-a773-f2184441c60e	You booking have been successful.	4f22ea07-52bd-43d3-9eac-fb6995389d4a	Hotel Name Kosal	Hotel name Native	booking 1 room that have 2 bed 2 day	booking 1 room that have 2 bed 2 day	1234	1234	pedding	2021-08-29	{"uri":"photo url"}
d702444a-fd9f-4f8e-9175-19f8ae210f12	You booking have been successful.	2899a00e-b32e-4c02-88aa-d1326dd81252	Baitong Hotel	សណ្តាគារបាយតុង	At Baitong Hotel we take great care to provide you a great hotel experience which includes the cleanliness of our hotel. To continue to provide an environment which is as safe as possible for you, and our team, 	we have taken extra steps to clean and sanitize our hotel more frequently. Every day we are following strict cleaning, quality control and hygiene procedures in all hotel areas.	0a375551-279e-45f7-931d-9b784132708c	cd925b7d-5136-4cd6-8708-9023fc6f97cc	pedding	2021-08-31	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/9d/84/ae/baitong-hotel-resort.jpg?w=900&h=-1&s=1"}
cd83eba1-97df-4dd2-9254-efdce764b3ad	You booking have been successful.	5855a3b1-ea3a-4820-9de6-d2a42e04d421	Baitong Hotel	សណ្តាគារបាយតុង	At Baitong Hotel we take great care to provide you a great hotel experience which includes the cleanliness of our hotel. To continue to provide an environment which is as safe as possible for you, and our team, 	we have taken extra steps to clean and sanitize our hotel more frequently. Every day we are following strict cleaning, quality control and hygiene procedures in all hotel areas.	0a375551-279e-45f7-931d-9b784132708c	cd925b7d-5136-4cd6-8708-9023fc6f97cc	pedding	2021-08-31	{"uri":"https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1a/9d/84/ae/baitong-hotel-resort.jpg?w=900&h=-1&s=1"}
\.


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payments (paymentid, bookingid, userid, name, namenative, description, descriptionnative, amount, created, updated) FROM stdin;
3b811d16-d73b-44bd-8614-0d8cf71d9d52	book room	book room	book 1 room that have 2 bed	book 1 room that have 2 bed	12344	1234	pedding	2021-08-16 00:00:00	\N
\.


--
-- Data for Name: places; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.places (placeid, name, namenative, description, photo, provinceid, toptreding, created, updated) FROM stdin;
5aca822f-6de4-4f84-b1f9-740984b8b25e	Wander Pub Street	ផ្សាររាត្រីក្រុងសៀមរាប	With so much culture, history, and ancient architecture to take in during the day, you might just want to let off some mindless steam at night. For a relaxed evening of western food and a wild night of Angkor beers on tap, head to Pub Street.	{"uri":"https://cdn.thecrazytourist.com/wp-content/uploads/2018/09/ccimage-shutterstock_635149646.jpg"}	5a8c8afa-5a09-43fb-8eb5-7832c102d184	yes	2021-08-21 00:00:00	\N
6ed8c0a7-bd3e-48c4-b3d8-20bd581ae9b8	Kulen Moutain	ភ្នំគូលេន	Talk to a tuk tuk driver to help you find a guide for the Kulen Nature Trails. These rainforest trails offer hidden gems in the rainforest such as ancient temples, respected monasteries, and rushing waterfalls.	{"uri":"https://cdn.thecrazytourist.com/wp-content/uploads/2018/09/ccimage-shutterstock_689409427.jpg"}	5a8c8afa-5a09-43fb-8eb5-7832c102d184	yes	2021-08-21 00:00:00	\N
dc589e22-25fa-4fc7-9d92-57138013f1c9	Banteay Srey Butterfly Centre	សួនមេអំបៅនៅបន្ទាយស្រី	Butterfly enthusiasts, nature lovers, and families with kids- here is an experience that you don’t want to pass up while in Cambodia. This butterfly sanctuary and conservation center is the perfect opportunity to get up close and personal	{"uri":"https://cdn.thecrazytourist.com/wp-content/uploads/2018/09/ccimage-shutterstock_1036195687.jpg"}	5a8c8afa-5a09-43fb-8eb5-7832c102d184	no	2021-08-21 00:00:00	\N
9b222cd4-0831-40ff-a091-0cd4037f50fb	Angkor Wat Temple	ប្រាសាទអង្គរវត្ត	Angkor Wat lies 5.5 kilometres (3+1⁄2 mi) north of the modern town of Siem Reap, and a short distance south and slightly east of the previous capital, which was centred at Baphuon.	{"uri":"https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ankor_Wat_temple.jpg/1200px-Ankor_Wat_temple.jpg"}	5a8c8afa-5a09-43fb-8eb5-7832c102d184	yes	2021-08-21 00:00:00	\N
c048523c-ea0b-4ff6-b62a-d06fdcb2a925	Royal Palace	ព្រះរាជវ៉ាំង	The highlight of Phnom Penh is the beautiful Royal Palace - the seat and residence of Cambodia's royal family since the 1860s. The spired-roof pavilions of the complex are an excellent example of classic Khmer architecture.	{"uri":"https://www.planetware.com/wpimages/2020/07/cambodia-phnom-penh-top-attractions-royal-palace.jpg"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	yes	2021-08-21 00:00:00	2021-08-21 00:00:00
6a864744-874d-435f-b124-1c2eeb0c7cdc	Cambodia National Museum	សារៈមន្ទីជាតិកម្ពុជា	The traditional Khmer building that houses the national collection was built in 1920. Inside, the dazzling array of ancient Khmer artistry, with more than 1,800 objects on display, is a must visit for anyone with an interest in Cambodian history.	{"uri":"https://www.planetware.com/photos-large/CAM/cambodia-phnom-penh-national-museum.jpg"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	yes	2021-08-21 00:00:00	\N
9561e3dd-cc01-4026-9b40-c9983b2d731a	Choeung Ek	ជើងឯក	The Killing Fields of Choeung Ek are a somber reminder of the terror of the Khmer Rouge whose brutal regime, intending to turn Cambodia into a socialist agrarian society, ruled the country between 1975 and early 1979.	{"uri":"https://www.planetware.com/photos-large/CAM/cambodia-phnom-penh-memorial-stupa-at-choeung-ek.jpg"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	yes	2021-08-21 00:00:00	\N
a8ad9151-69d0-4088-b84d-fb9f72c085b7	Tuol Sleng Museum	សារៈមន្ទីទួលស្លែង	It was here, in the Khmer Rouge's Security Prison S-21 that some of the regime's worst torture atrocities were carried out. More than 17,000 people passed through these gates between 1975 and 1978, accused of betraying the revolution in some way.	{"uri":"https://www.planetware.com/photos-large/CAM/cambodia-phnom-penh-tuol-sleng-museum.jpg"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	no	2021-08-21 00:00:00	\N
c4ff14e0-308d-42f6-9a14-bda9dc145441	River Boat Cruises	ជិះទូកមើលទីក្រុងភ្នំពេញ	Phnom Penh is a riverine town, and one of the most relaxing ways to go sightseeing in the city is to take to the water. There are regular sunset cruise tourist boat departures from the riverfront between 5pm and 7.30pm,	{"uri":"https://www.planetware.com/photos-large/CAM/cambodia-phnom-penh-boat-tour-on-mekong-river.jpg"}	2f3be322-f07e-4ea5-badf-d6b89fd4eeba	yes	2021-08-21 00:00:00	\N
03d22263-53dc-4c64-a7e5-af1b65b3b5f7	Bayon Temple	ប្រាសាទបាយ័ន	The Bayon was the last state temple to be built at Angkor (Khmer: ក្រុងអង្គរ), and the only Angkorian state temple to be built primarily to worship Brahma, though a great number of minor and local deities were also encompassed as representatives	{"uri":"https://image.shutterstock.com/image-photo/bayon-temple-wellknown-richly-decorated-600w-1062932165.jpg"}	5a8c8afa-5a09-43fb-8eb5-7832c102d184	no	2021-08-21 00:00:00	\N
\.


--
-- Data for Name: provinces; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.provinces (provinceid, name, namenative, created, updated) FROM stdin;
2f3be322-f07e-4ea5-badf-d6b89fd4eeba	Phnom Penh	ភ្នំពេញ	2021-08-21 00:00:00	\N
5a8c8afa-5a09-43fb-8eb5-7832c102d184	Siem Reap	សៀមរាប	2021-08-21 00:00:00	\N
96115cd3-9730-4f90-bfa2-c4e6a5be9bd4	Battambang	បាត់ដំបង	2021-08-21 00:00:00	\N
097f14c7-1225-4aa2-998c-7af16cf64e09	Banteay Meanchey	បន្ទាយមានជ័យ	2021-08-21 00:00:00	\N
a228f4a4-ab84-4342-a0d3-6adc3f89f0ee	Kampong Cham	កំពង់ចាម	2021-08-21 00:00:00	\N
f4700e4f-1388-405c-8a56-72aca51601a9	Kampong Chhnang	កំពង់ឆ្នាំង	2021-08-21 00:00:00	\N
6b51bfe1-ee4e-48ca-a59b-0d8ada094580	Kampong Speu	កំពង់ស្ពឺ	2021-08-21 00:00:00	\N
6fa98a1c-9d2b-44ca-acf9-19fff1c8f7ce	Kampong Thom	កំពង់ធំ	2021-08-21 00:00:00	\N
a5da4570-cef1-4233-bfa8-73b7c602aec2	Kampot	កំពត	2021-08-21 00:00:00	\N
0745f27e-e83b-41d2-9a6e-7cb43d2d0e8a	Kandal	កណ្តាល	2021-08-21 00:00:00	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (userid, firstname, lastname, photo, email, phone, password, address, created, updated, deleted, gender) FROM stdin;
31eaa220-63cb-4f41-9bc7-08bbdac73027	Chhorn	Soktheng	{"uri":"avatar.png"}	kosal@gmail.com	+855969604049	1234	phnom penh	2021-08-21 00:00:00	\N	\N	male
cd925b7d-5136-4cd6-8708-9023fc6f97cc	YaYa	Yamaha	{"uri":"https://scontent.fpnh1-1.fna.fbcdn.net/v/t1.6435-9/49319484_1225324064281359_1753021185676804096_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=ELJ5Lxss7hgAX_w__Zm&_nc_ht=scontent.fpnh1-1.fna&oh=6cc31166ab9c6e151001b047719b6e4e&oe=61491583"}	yaya@gmail.com	+855969604049	1234	phnom penh	2021-08-16 00:00:00	2021-08-22 00:00:00	\N	female
\.


--
-- Name: bookings booking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bookings
    ADD CONSTRAINT booking_pkey PRIMARY KEY (bookingid);


--
-- Name: comments comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comment_pkey PRIMARY KEY (commentid);


--
-- Name: favorite_hotels favorite_hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favorite_hotels
    ADD CONSTRAINT favorite_hotel_pkey PRIMARY KEY (favoritehotelid);


--
-- Name: favorite_places favorite_place_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.favorite_places
    ADD CONSTRAINT favorite_place_pkey PRIMARY KEY (favoriteplaceid);


--
-- Name: hotels hotel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hotels
    ADD CONSTRAINT hotel_pkey PRIMARY KEY (hotelid);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (notificationid);


--
-- Name: payments payment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payment_pkey PRIMARY KEY (paymentid);


--
-- Name: places place_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.places
    ADD CONSTRAINT place_pkey PRIMARY KEY (placeid);


--
-- Name: provinces province_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provinces
    ADD CONSTRAINT province_pkey PRIMARY KEY (provinceid);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


--
-- PostgreSQL database dump complete
--

