const addUser =
  "insert into users (userId, firstname, lastname, photo, email, phone, password, address, created, gender) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)";
const getUsers = "select * from users";
const getUserById = "select * from users where userId = $1";
const updateUser =
  "update users set firstname = $1, lastname = $2, email = $3, phone = $4, address = $5, updated = $6, gender = $7, photo = $8 where userId = $9";
const deleteUser = "delete from users where userId = $1";
const checkEmailExists = "select s from users s where s.email = $1";

module.exports = {
  addUser,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
  checkEmailExists,
};
