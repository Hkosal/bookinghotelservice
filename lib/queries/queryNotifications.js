const addNotification =
  "insert into notifications (notificationId, notificationDescription, bookingId, name, nameNative, description, descriptionNative, photo, hotelId, userId, status, created) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)";
const getNotifications = "select * from notifications";
const getNotificationById =
  "select * from notifications where notificationId = $1";
const deleteNotification =
  "delete from notifications where notificationId = $1";
const notificationByUser = "select * from notifications where userId = $1";

module.exports = {
  addNotification,
  getNotifications,
  getNotificationById,
  deleteNotification,
  notificationByUser,
};
