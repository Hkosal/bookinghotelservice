const addPlace =
  "insert into places (placeId, name, nameNative, description, photo, provinceId, topTreding, created) values ($1, $2, $3, $4, $5, $6, $7, $8)";
const getPlaces = "select * from places";
const getPlaceById = "select * from places where placeId = $1";
const updatePlace =
  "update places set name = $1, nameNative = $2, description = $3, photo = $4, provinceId = $5, topTreding = $6, updated = $7 where placeId = $8";
const deletePlace = "delete from places where placeId = $1";
const getTopTredings = "select * from places where topTreding = $1";
const getPlacesInProvince = "select * from places where provinceId = $1";
const searchByName = 'SELECT * FROM places WHERE name LIKE "%$1%"';

module.exports = {
  addPlace,
  getPlaces,
  getPlaceById,
  searchByName,
  updatePlace,
  deletePlace,
  getTopTredings,
  getPlacesInProvince,
};
