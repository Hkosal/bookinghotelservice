const addPayment =
  "insert into payments (paymentId, bookingId, userId, name, nameNative, description, descriptionNative, amount, created) values ($1, $2, $3, $4, $5, $6, $7, $8, $9)";
const getPayments = "select * from payments";
const getPaymentById = "select * from payments where paymentId = $1";
const updatePayment =
  "update payments set bookingId = $1, userId = $2, name = $3, nameNative = $4, description = $5, descriptionNative = $6, amount = $7, updated = $8 where paymentId = $9";
const deletePayment = "delete from payments where paymentId = $1";

module.exports = {
  addPayment,
  getPayments,
  getPaymentById,
  updatePayment,
  deletePayment,
};
