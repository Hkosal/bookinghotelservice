const addHotel =
  "insert into hotels (hotelId, name, nameNative, description, descriptionNative, photo, placeId, provinceId, created) values ($1, $2, $3, $4, $5, $6, $7, $8, $9)";
const getHotels = "select * from hotels";
const getHotelById = "select * from hotels where hotelId = $1";
const updateHotel =
  "update hotels set name = $1, nameNative = $2, description = $3, descriptionNative = $4, photo = $5, placeId = $6, provinceId = $7, updated = $8 where hotelId = $9";
const deleteHotel = "delete from hotels where hotelId = $1";
const getHotelsInProvince = "select * from hotels where provinceId = $1";
const getHotelsInPlace = "select * from hotels where placeId = $1";

module.exports = {
  addHotel,
  getHotels,
  getHotelById,
  updateHotel,
  deleteHotel,
  getHotelsInProvince,
  getHotelsInPlace,
};
