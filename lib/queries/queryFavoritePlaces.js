const addFavoritePlace =
  "insert into favorite_places (favoritePlaceId, userId, placeId, name, nameNative, description, photo, provinceId, topTreding, created) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)";
const getFavoritePlaces = "select * from favorite_places";
const getFavoritePlaceById =
  "select * from favorite_places where favoritePlaceId = $1";
const getFavoritePlacesByUser =
  "select * from favorite_places where userId = $1";
const updateFavoritePlace =
  "update favorite_places set userId = $1, placeId = $2 where favoritePlaceId = $3";
const deleteFavoritePlace =
  "delete from favorite_places where favoritePlaceId = $1";

module.exports = {
  addFavoritePlace,
  getFavoritePlaces,
  getFavoritePlaceById,
  updateFavoritePlace,
  deleteFavoritePlace,
  getFavoritePlacesByUser,
};
