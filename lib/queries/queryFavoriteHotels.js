const addFavoriteHotel =
  "insert into favorite_hotels (favoriteHotelId, userId, hotelId, name, nameNative, description, descriptionNative, photo, provinceId, placeId, created) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)";
const getFavoriteHotels = "select * from favorite_hotels";
const getFavoriteHotelById =
  "select * from favorite_hotels where favoriteHotelId = $1";
const getFavoriteHotelsByUser =
  "select * from favorite_hotels where userId = $1";
const updateFavoriteHotel =
  "update favorite_hotels set userId = $1, hotelId = $2 where favoriteHotelId = $3";
const deleteFavoriteHotel =
  "delete from favorite_hotels where favoriteHotelId = $1";

module.exports = {
  addFavoriteHotel,
  getFavoriteHotels,
  getFavoriteHotelById,
  updateFavoriteHotel,
  deleteFavoriteHotel,
  getFavoriteHotelsByUser,
};
