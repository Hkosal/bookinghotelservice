const addComment =
  "insert into comments (commentId, userId, firstname, lastname, phone, created) values ($1, $2, $3, $4, $5, $6)";
const getComments = "select * from comments";
const getCommentById = "select * from comments where commentId = $1";
const updateComment =
  "update comments set userId = $1, firstname = $2, lastname = $3, phone = $4, updated = $5 where commentId = $6";
const deleteComment = "delete from comments where commentId = $1";

module.exports = {
  addComment,
  getComments,
  getCommentById,
  updateComment,
  deleteComment,
};
