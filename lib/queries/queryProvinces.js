const addProvince =
  "insert into provinces (provinceId, name, nameNative, created) values ($1, $2, $3, $4)";
const getProvinces = "select * from provinces";
const getProvinceById = "select * from provinces where provinceId = $1";
const updateProvince =
  "update provinces set name = $1, nameNative = $2, updated = $3 where provinceId = $4";
const deleteProvince = "delete from provinces where provinceId = $1";

module.exports = {
  addProvince,
  getProvinces,
  getProvinceById,
  updateProvince,
  deleteProvince,
};
