const addBooking =
  "insert into bookings (bookingId, name, nameNative, description, descriptionNative, photo, hotelId, userId, status, created) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)";
const getBookings = "select * from bookings";
const getBookingById = "select * from bookings where bookingId = $1";
const updateBooking =
  "update bookings set name = $1, nameNative = $2, description = $3, descriptionNative = $4, hotelId = $5, userId = $6, status = $7, updated = $8 where bookingId = $9";
const deleteBooking = "delete from bookings where bookingId = $1";
const getBookingByStatus = "select * from bookings where status = $1";

module.exports = {
  addBooking,
  getBookings,
  getBookingById,
  updateBooking,
  deleteBooking,
  getBookingByStatus,
};
